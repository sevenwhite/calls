# The application for managing TODO list of calls

The task description you can see in `Interview Application Dev JS.pdf`.

You can see the [demo](https://sevenwhite.gitlab.io/calls/).

### Start
`$ npm start`

### Test
`$ npm test`