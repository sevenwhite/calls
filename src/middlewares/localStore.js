export default store => next => action => {
  next(action)
  localStorage.setItem('state', JSON.stringify(store.getState()))
}