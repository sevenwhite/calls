import React, { Component } from 'react';
import './App.css';
import {Col, Grid, Row} from "react-bootstrap";
import AddCallPanel from "./containers/AddCallPanel";
import NextCallPanel from "./containers/NextCallPanel";
import CallsTable from "./containers/CallsTable";
import FilterButtons from "./containers/FilterButtons";
import { library } from '@fortawesome/fontawesome-svg-core'
import { faSortUp, faSortDown, faSort } from '@fortawesome/free-solid-svg-icons'

library.add(faSortUp, faSortDown, faSort)

class App extends Component {
  render() {
    return (
      <Grid>
        <br/>
        <Row className="show-grid">
          <Col xs={6} md={6}>
            <NextCallPanel/>
          </Col>
          <Col xs={6} md={6}>
            <AddCallPanel/>
          </Col>
        </Row>
        <Row>
          <Col xs={5} md={5}/>
          <Col xs={7} md={7}>
            <CallsTable/>
          </Col>
        </Row>
        <Row>
          <Col xs={5} md={5}/>
          <Col xs={7} md={7}>
            <FilterButtons />
          </Col>
        </Row>
      </Grid>
    );
  }
}

export default App;
