import moment from "moment/moment";

export function extractPhone(value) {
  const regexp = /\d/g
  let numbers = value.match(regexp)
  
  if (numbers.length < 14) {
    numbers = ['0', '0', ...numbers]
  }
  
  numbers.splice(5, 0, ' ')
  numbers.splice(9, 0, ' ')
  numbers.splice(13, 0, ' ')
  
  return numbers.join('')
}

export function expired(time, now) {
  // for tests
  if (!now) {
    now = moment()
  }
  return moment(time, 'h:m').isBefore(now)
}