import {extractPhone, expired} from './index'
import moment from "moment";

it('extract phone', () => {
  const phone = '00420 111 222 333'
  expect(extractPhone('+(420) 111 222 333')).toEqual(phone)
  expect(extractPhone('+(420)-111222333')).toEqual(phone)
  expect(extractPhone('+420111222333')).toEqual(phone)
  expect(extractPhone('00420111222333')).toEqual(phone)
})

it('expired time', () => {
  const now = moment('21:04', 'h:m')
  expect(expired('20:20', now)).toEqual(true)
  expect(expired('21:05', now)).toEqual(false)
})