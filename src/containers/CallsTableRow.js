import React, {Component} from 'react'
import {connect} from 'react-redux'
import Call from "../propTypes/Call"
import {Checkbox} from "react-bootstrap"
import {expired} from "../utils"
import {removeCall} from "../actions"
import PropTypes from 'prop-types'

class CallsTableRow extends Component {
  static propTypes = {
    call: Call.isRequired,
    
    removeCall: PropTypes.func.isRequired,
  }
  render() {
    const {call} = this.props
    
    return <tr>
      <td>{call.name}</td>
      <td>{call.phone}</td>
      <td>{call.time}</td>
      <td><a onClick={this.onDelete}>Delete</a></td>
      <td>
        <Checkbox checked={expired(call.time)} disabled inline/>
      </td>
    </tr>
  }
  constructor(props) {
    super(props)
    
    this.onDelete = this.onDelete.bind(this)
  }
  onDelete(e) {
    e.preventDefault()
    
    const {call, removeCall} = this.props
    return removeCall(call.id)
  }
}

export default connect(
  () => ({}),
  dispatch => ({
    removeCall(id) {
      return dispatch(removeCall(id))
    }
  })
)(CallsTableRow)