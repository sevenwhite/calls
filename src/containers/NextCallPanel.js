import React, {Component} from 'react'
import {connect} from 'react-redux'
import {Form, FormControl, Panel} from "react-bootstrap"
import {nextCalls} from "../selectors"
import Call from "../propTypes/Call"
import PropTypes from 'prop-types'

class NextCallPanel extends Component {
  static propTypes = {
    calls: PropTypes.arrayOf(Call).isRequired,
  }
  render() {
    const {calls} = this.props
    const nextCall = (calls.length) ? calls[0] : {}
    return <Panel>
      <Panel.Heading>
        <Panel.Title componentClass="h3">Next call</Panel.Title>
      </Panel.Heading>
      <Panel.Body>
        <Form inline onSubmit={() => {}}>
          <FormControl
            type="text"
            defaultValue={nextCall.name}
            bsSize="sm"
            disabled
          />&nbsp;
          <FormControl
            type="text"
            defaultValue={nextCall.phone}
            bsSize="sm"
            disabled
          />&nbsp;
          <FormControl
            type="text"
            defaultValue={nextCall.time}
            bsSize="sm"
            disabled
          />&nbsp;
        </Form>
      </Panel.Body>
    </Panel>
  }
}

export default connect(
  state => ({
    calls: nextCalls(state)
  })
)(NextCallPanel)