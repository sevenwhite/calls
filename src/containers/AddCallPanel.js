import React, {Component} from 'react'
import {connect} from 'react-redux'
import {Panel} from "react-bootstrap"
import AddCallForm from "../components/AddCallForm"
import {addCall} from "../actions"
import PropTypes from 'prop-types'
import {reset} from 'redux-form'

class AddCallPanel extends Component {
  static propTypes = {
    addCall: PropTypes.func.isRequired,
    resetForm: PropTypes.func.isRequired,
  }
  render() {
    return <Panel>
      <Panel.Heading>
        <Panel.Title componentClass="h3">Add call</Panel.Title>
      </Panel.Heading>
      <Panel.Body>
        <AddCallForm onSubmit={this.handleSubmit}/>
      </Panel.Body>
    </Panel>
  }
  constructor(props) {
    super(props)
    
    this.handleSubmit = this.handleSubmit.bind(this)
  }
  handleSubmit(values) {
    const {name, phone, time} = values
    this.props.addCall(name, phone, time)
    this.props.resetForm()
  }
}

export default connect(
  () => ({}),
  dispatch => ({
    addCall(name, phone, time) {
      return dispatch(addCall(name, phone, time))
    },
    resetForm() {
      return dispatch(reset("AddCall"))
    }
  })
)(AddCallPanel)