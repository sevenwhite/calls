import React, {Component} from 'react'
import {Button, ButtonToolbar} from "react-bootstrap";
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import {FILTER_VALUES} from "../constants";
import {setFilter} from "../actions";


class FilterButtons extends Component {
  static propTypes = {
    setFilter: PropTypes.func.isRequired
  }
  render() {
    return <ButtonToolbar className="btn-center">
      <Button type="button" onClick={this.all}>All</Button>
      <Button type="button" onClick={this.next}>Next</Button>
      <Button type="button" onClick={this.finished}>Finished</Button>
    </ButtonToolbar>
  }
  constructor(props) {
    super(props)
    
    this.all = this.all.bind(this)
    this.next = this.next.bind(this)
    this.finished = this.finished.bind(this)
  }
  all() {
    this.props.setFilter(FILTER_VALUES.ALL)
  }
  next() {
    this.props.setFilter(FILTER_VALUES.NEXT)
  }
  finished() {
    this.props.setFilter(FILTER_VALUES.FINISHED)
  }
}

export default connect(
  () => ({}),
  dispatch => ({
    setFilter(value) {
      return dispatch(setFilter(value))
    }
  })
)(FilterButtons)