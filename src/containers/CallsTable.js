import React, {Component} from 'react'
import {connect} from 'react-redux'
import {Table} from "react-bootstrap"
import CallsTableRow from "./CallsTableRow"
import PropTypes from 'prop-types'
import Call from "../propTypes/Call";
import {sort, sortedCalls} from "../selectors";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome/index.es";
import {SORT} from "../constants";
import {setSort} from "../actions";


class CallsTable extends Component {
  static propTypes = {
    sort: PropTypes.string.isRequired,
    calls: PropTypes.arrayOf(Call).isRequired,
  
    setSort: PropTypes.func.isRequired,
  }
  render() {
    const {calls} = this.props
    
    return <Table responsive>
      <thead>
      <tr>
        <th onClick={this.toggleNameSort}>Name {this.getNameIcon()}</th>
        <th>Phone number</th>
        <th onClick={this.toggleTimeSort}>Time {this.getTimeIcon()}</th>
        <th/>
        <th/>
      </tr>
      </thead>
      <tbody>
      {calls.map(call => <CallsTableRow call={call} key={call.id} />)}
      </tbody>
    </Table>
  }
  constructor(props) {
    super(props)
    
    this.toggleNameSort = this.toggleNameSort.bind(this)
    this.toggleTimeSort = this.toggleTimeSort.bind(this)
  }
  toggleNameSort() {
    const {setSort, sort} = this.props
    
    if (sort === SORT.NAME_ASK) {
      return setSort(SORT.NAME_DESC)
    }
    
    return setSort(SORT.NAME_ASK)
  }
  toggleTimeSort() {
    const {setSort, sort} = this.props
    
    if (sort === SORT.TIME_ASK) {
      return setSort(SORT.TIME_DESC)
    }
    
    return setSort(SORT.TIME_ASK)
  }
  getNameIcon() {
    switch (this.props.sort) {
      case SORT.NAME_DESC:
        return <FontAwesomeIcon icon="sort-down" />
      case SORT.NAME_ASK:
        return <FontAwesomeIcon icon="sort-up" />
      default:
        return <FontAwesomeIcon icon="sort" />
    }
  }
  getTimeIcon() {
    switch (this.props.sort) {
      case SORT.TIME_DESC:
        return <FontAwesomeIcon icon="sort-down" />
      case SORT.TIME_ASK:
        return <FontAwesomeIcon icon="sort-up" />
      default:
        return <FontAwesomeIcon icon="sort" />
    }
  }
}

export default connect(
  (state) => ({
    calls: sortedCalls(state),
    sort: sort(state),
  }),
  dispatch => ({
    setSort(value) {
      return dispatch(setSort(value))
    }
  })
)(CallsTable)