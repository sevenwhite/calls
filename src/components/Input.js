import React, {Component} from "react";
import {FormControl, FormGroup, HelpBlock} from "react-bootstrap";

export default class Input extends Component {
  render() {
    const { input, meta: { touched, error }, type, placeholder, bsSize, min, max} = this.props
  
    return <FormGroup validationState={touched && error ? "error" : "success"}>
      <FormControl
        id={input.name}
        type={type}
        placeholder={placeholder}
        min={min}
        max={max}
        value={input.value}
        bsSize={bsSize}
        onChange={input.onChange} />
      {touched && error ? <HelpBlock>{error}</HelpBlock> : <HelpBlock>{placeholder}</HelpBlock> }
    </FormGroup>
  }
}