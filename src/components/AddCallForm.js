import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Button, Form} from "react-bootstrap";
import {Field, reduxForm} from 'redux-form'
import Input from "./Input";
import {maxLength, phone, required, time} from "../validators";


class AddCallForm extends Component {
  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
  }
  render() {
    const {handleSubmit, pristine, submitting} = this.props
    return <Form inline onSubmit={handleSubmit}>
      <Field
        type="text"
        component={Input}
        name="name"
        placeholder="name"
        validate={[required, maxLength(15)]}
        bsSize="sm"
      />&nbsp;
      <Field
        type="text"
        name="phone"
        component={Input}
        placeholder="phone number"
        validate={[required, phone]}
        bsSize="sm"
      />&nbsp;
      <Field
        type="text"
        component={Input}
        name="time"
        placeholder="time"
        validate={[required, time]}
        bsSize="sm"
      />&nbsp;
      <br/>
      <br/>
      <Button type="submit" disabled={pristine || submitting}>Add</Button>
    </Form>
  }
}

export default reduxForm({form: "AddCall"})(AddCallForm)

