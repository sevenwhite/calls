import moment from 'moment'

export const required = value => (value ? undefined : 'Required')

export const maxLength = max => value =>
  value && value.length > max ? `Must be ${max} characters or less` : undefined

export const time = value => (
  (/^\d{2}:\d{2}$/.test(value) && moment(value, 'h:m').isValid())
    ? undefined : "must be in mm:ss format")

export const phone = value => {
  const errMsg = "must be in 00420111222333 format"
  value = value.replace(/\s/g, '')
  
  const regexps = [
    /^(\+|00)\(\d{3}\)-?\d{9}$/,
    /^(\+|00)\d{3}-?\d{9}$/,
  ]
  
  for (let regexp of regexps) {
    // console.log('value', value, value.match(regexp))
    if (regexp.test(value)) {
      return undefined
    }
  }
  
  return errMsg
}