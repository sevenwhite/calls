import {time, phone} from './index'

it('time validator', () => {
  expect(time('11:12')).toEqual(undefined)
  expect(time('111:122')).not.toEqual(undefined)
  expect(time('34:22')).not.toEqual(undefined)
  expect(time('34-22')).not.toEqual(undefined)
  expect(time('3422')).not.toEqual(undefined)
})

it('phone validator', () => {
  expect(phone('+(420) 111 222 333')).toEqual(undefined)
  expect(phone('1+(420) 111 222 333')).not.toEqual(undefined)
  expect(phone('+(420) 111 222 333 4')).not.toEqual(undefined)
  expect(phone('+(420)-111222333')).toEqual(undefined)
  expect(phone('+420111222333')).toEqual(undefined)
  expect(phone('00420111222333')).toEqual(undefined)
  expect(phone('420111222333')).not.toEqual(undefined)
  expect(phone('0420111222333')).not.toEqual(undefined)
  expect(phone('+123')).not.toEqual(undefined)
  expect(phone('+fff')).not.toEqual(undefined)
  expect(phone('+(420)­111-222-333')).not.toEqual(undefined)
  expect(phone('+(420)-1112ff22333')).not.toEqual(undefined)
})