import {applyMiddleware, compose, createStore} from 'redux'
import reducers from '../reducers'
import localStore from "../middlewares/localStore";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
let previousState = localStorage.getItem('state')
previousState = previousState ? JSON.parse(previousState) : {}

export default createStore(reducers, previousState, composeEnhancers(
  applyMiddleware(localStore)
))