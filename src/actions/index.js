import {ADD_CALL, REMOVE_CALL, SET_FILTER, SET_SORT} from "../actionTypes";

export function addCall(name, phone, time) {
  return {
    type: ADD_CALL,
    name,
    phone,
    time,
  }
}

export function removeCall(id) {
  return {
    type: REMOVE_CALL,
    id,
  }
}

export function setFilter(value) {
  return {
    type: SET_FILTER,
    value,
  }
}

export function setSort(value) {
  return {
    type: SET_SORT,
    value,
  }
}