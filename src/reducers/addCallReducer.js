import uuid from "uuid/v4";
import {extractPhone} from "../utils";

export default function(state, action) {
  const {name, phone, time} = action
  const call = {id: uuid(), name, phone: extractPhone(phone), time}
  return [...state, call]
}