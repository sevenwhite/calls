import addCallReducer from "./addCallReducer"
import {addCall, removeCall} from "../actions";
import removeCallReducer from "./removeCallReducer";

it('calls reducer', () => {
  expect(addCallReducer([], addCall('John', '00420112233', '20:20')).length).toEqual(1)
  expect(removeCallReducer([{id: 1}], removeCall(1)).length).toEqual(0)
})