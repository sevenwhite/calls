import {SET_SORT} from "../actionTypes"
import {createReducer} from "./index"
import {SORT} from "../constants";

const initialState = SORT.TIME_ASK

export default createReducer(initialState, {
  [SET_SORT]: (state, action) => action.value,
})