import {combineReducers} from "redux"
import {reducer as form} from 'redux-form'
import calls from "./calls";
import filter from "./filter";
import sort from "./sort";

export function createReducer(initialState, handlers) {
  return (state = initialState, action) => {
    if (handlers.hasOwnProperty(action.type)) {
      return handlers[action.type](state, action)
    } else {
      return state
    }
  }
}

export default combineReducers({
  form,
  calls,
  filter,
  sort,
})