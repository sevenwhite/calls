import {SET_FILTER} from "../actionTypes"
import {createReducer} from "./index"
import {FILTER_VALUES} from "../constants";

const initialState = FILTER_VALUES.ALL

export default createReducer(initialState, {
  [SET_FILTER]: (state, action) => action.value,
})