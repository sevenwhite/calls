import {ADD_CALL, REMOVE_CALL} from "../actionTypes"
import {createReducer} from "./index"
import addCallReducer from "./addCallReducer";
import removeCallReducer from "./removeCallReducer";

const initialState = []

export default createReducer(initialState, {
  [ADD_CALL]: addCallReducer,
  [REMOVE_CALL]: removeCallReducer,
})