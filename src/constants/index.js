export const FILTER_VALUES = {
  ALL: 'all',
  NEXT: 'next',
  FINISHED: 'finished'
}

export const SORT = {
  TIME_ASK: 'time asc',
  TIME_DESC: 'time desc',
  NAME_ASK: 'name ask',
  NAME_DESC: 'name desc'
}