import {createSelector} from "reselect"
import moment from 'moment'
import {expired} from "../utils";
import {FILTER_VALUES, SORT} from "../constants";

export const calls = state => state.calls
export const filter = state => state.filter
export const sort = state => state.sort


export const nextCalls = createSelector(
  calls,
  calls => calls.filter(call => !expired(call.time))
)

export const finishedCalls = createSelector(
  calls,
  calls => calls.filter(call => expired(call.time))
)

export const filteredCalls = createSelector(
  filter, calls, nextCalls, finishedCalls,
  (filter, calls, nextCalls, finishedCalls) => {
    switch (filter) {
      case FILTER_VALUES.NEXT:
        return nextCalls
      case FILTER_VALUES.FINISHED:
        return finishedCalls
      default:
        return calls
    }
  }
)

export const callsByTimeASC = createSelector(
  filteredCalls,
  calls => [...calls].sort((a, b) => moment(a.time, 'h:m').unix() - moment(b.time, 'h:m').unix())
)

export const callsByNameASC = createSelector(
  filteredCalls,
  calls => [...calls].sort((a, b) => {
    if (a.name > b.name) {
      return 1;
    }
    if (a.name < b.name) {
      return -1;
    }
    // a должно быть равным b
    return 0;
  })
)

export const callsByTimeDESC = createSelector(
  filteredCalls,
  calls => [...calls].sort((a, b) => moment(b.time, 'h:m').unix() - moment(a.time, 'h:m').unix())
)

export const sortedCalls = createSelector(
  sort, callsByTimeASC, callsByTimeDESC, callsByNameASC,
  (sort, callsByTimeASC, callsByTimeDESC, callsByNameASC) => {
    switch (sort) {
      case SORT.NAME_DESC:
        return [...callsByNameASC].reverse()
      case SORT.NAME_ASK:
        return callsByNameASC
      case SORT.TIME_DESC:
        return callsByTimeDESC
      default:
        return callsByTimeASC
    }
  }
)