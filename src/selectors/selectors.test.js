import {callsByTimeASC, callsByTimeDESC} from "./index";

it('callsByTimeASC selector', () => {
  let state = {
    calls: [{time: '20:20'}, {time: '14:20'}]
  }
  expect(callsByTimeASC(state)[0].time).toEqual('14:20')
  
  state = {
    calls: [{time: '14:20'}, {time: '20:20'}]
  }
  expect(callsByTimeDESC(state)[0].time).toEqual('20:20')
})