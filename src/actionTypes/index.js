export const ADD_CALL = 'ADD_CALL'
export const REMOVE_CALL = 'REMOVE_CALL'
export const SET_FILTER = 'SET_FILTER'
export const SET_SORT = 'SET_SORT'